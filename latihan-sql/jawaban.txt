1.buat Database

 create database myshop;

2. create table
create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

3.insert table
insert into users(name,email,password) values
    -> ("Jhon Doe","jhon@doe.com","jhon123");

insert into users(name,email,password) values
    -> ("Jane Doe","jane@doe.com","jenita123");

insert into categories(name) values
    -> ("gadet"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");

insert into items(name,description,price,stock,category_id) values
    -> ("sumsang b50","hape keren dari merek sumsang",4000000,100,1),
    -> ("Uniklooh","baju dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. mengambil data 
	a.data user
	select name, email from users;

	b.data items
	select * from items where price > 1000000;
	select * from items where name like "%uniklo%";
	
	c. data items join
	select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id=categories.id;

5. UBAH DATA
	update items set price=2500000 where name like "%sumsang%";

.....