<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $animal= new animal("shaun");
    echo "nama hewan= $animal->name<br>";
    echo "jumlah kaki=  $animal->foot<br>";
    echo "berdarah dingin = $animal->cold_blooded<br>";
    echo "<br>";

    $frog= new frog("buduk");
    echo "nama hewan= $frog->name<br>";
    echo "jumlah kaki=  $frog->foot<br>";
    echo "berdarah dingin = $frog->cold_blooded<br>";
    echo $frog->jump();
    echo "<br><br>";

    $ape= new ape("kera sakti");
    echo "nama hewan= $ape->name<br>";
    echo "jumlah kaki=  $ape->foot<br>";
    echo "berdarah dingin = $ape->cold_blooded<br>";
    echo $ape->yell();
    echo "<br><br>";

?>